from rest_framework.test import APIClient, APITestCase
from rest_framework import status
from django.shortcuts import reverse
from random import randint
from time import time
from django.test import tag
from collections import OrderedDict

from .models import Bunny, Vegetable
from django.contrib.auth.models import User

# Create your tests here.

"""
Registration json
{
    "name" : "Bunneh",
    "vegetables": [
        {
            "vegetableType": "carrots",
            "value": 50
        }],
    "userAccount" : {
        "username" : "bunneh",
        "password" : "123"
    }
}
"""


def registerDummyBunny(name="Test", username="test", password="123", vegetableType="carrots", vegetableValue=50):
    user = User(username=username)
    user.set_password(password)
    user.save()
    bunny = Bunny(name=name, userAccount=user)
    bunny.save()
    veg = Vegetable(vegetableType=vegetableType, value=vegetableValue, bunny=bunny)
    veg.save()
    return bunny


def register20000Bunnies():
    for i in range(20000):
        if i % 1000 == 0:
            print("Created %d bunnies" % i)

        name = "Test" + str(i)
        username = "test" + str(i)
        vegetableNumber = randint(1, 10000)

        user = User(username=username)
        user.set_password("123")
        user.save()
        bunny = Bunny(name=name, userAccount=user)
        bunny.save()
        veg = Vegetable(vegetableType="carrots", value=vegetableNumber, bunny=bunny)
        veg.save()


class ListTests(APITestCase):
    client = APIClient()

    def test_empty_list(self):
        url = reverse("api:list", args=["carrots"])
        response = self.client.get(url)

        self.assertEquals(response.data, {'No bunnies': 'there is 0 bunnies with this vegetable'})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_list_no_such_vegetable(self):
        url = reverse("api:list", args=["carrotswrong"])
        response = self.client.get(url)

        self.assertEquals(response.data, ['No such vegetable. Available are: carrots'])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list_three_bunnies_sorted(self):
        registerDummyBunny(username="test1", vegetableValue=35)
        registerDummyBunny(username="test2", vegetableValue=25)
        registerDummyBunny(username="test3", vegetableValue=45)

        url = reverse("api:list", args=["carrots"])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data,
                         [OrderedDict([('name', 'Test'), ('vegetables', {'vegetableType': 'carrots', 'value': 45})]),
                          OrderedDict([('name', 'Test'), ('vegetables', {'vegetableType': 'carrots', 'value': 35})]),
                          OrderedDict([('name', 'Test'), ('vegetables', {'vegetableType': 'carrots', 'value': 25})])])


@tag("slow")
class ListPerformanceTests(APITestCase):
    fixtures = ["20000bunnies.json"]

    def test_if_20000_bunnies_exist(self):
        self.assertEqual(Bunny.objects.count(), 20000)

    def test_20000_bunnies_performance(self):
        timestart = time()

        url = reverse("api:list", args=["carrots"])
        with self.assertNumQueries(1):
            response = self.client.get(url)

        timeMeasured = time() - timestart
        print("Sorted bunnies. Took: " + str(timeMeasured))

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AccountTests(APITestCase):
    client = APIClient()

    def test_register_single_user(self):
        name = "Tester"
        username = "tester"
        password = "123"
        vegetablesList = [{"vegetableType": "carrots", "value": 50}]
        data = {"name": name,
                "vegetables": vegetablesList,
                "userAccount": {"username": username, "password": password}}

        url = reverse("api:register")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)
        self.assertEqual(Bunny.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Vegetable.objects.count(), 1)

    def test_register_single_user_no_vegetable(self):
        name = "Tester"
        username = "tester"
        password = "123"
        data = {"name": name,
                "userAccount": {"username": username, "password": password}}

        url = reverse("api:register")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)
        self.assertEqual(Bunny.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Vegetable.objects.count(), 0)

    def test_register_single_user_with_typo(self):
        name = "Tester"
        username = "tester"
        password = "123"
        vegetablesList = [{"vegetableType": "carrots", "value": 50}]
        data = {"name": name,
                "vegetables": vegetablesList,
                "userAccunt": {"username": username, "password": password}}

        url = reverse("api:register")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'userAccount': ['This field is required.']})
        self.assertEqual(Bunny.objects.count(), 0)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(Vegetable.objects.count(), 0)

    def test_register_duplicate_user(self):
        """tests if unable to register duplicate username"""
        name = "Tester"
        username = "tester"
        password = "123"
        vegetablesList = [{"vegetableType": "carrots", "value": 50}]
        data = {"name": name,
                "vegetables": vegetablesList,
                "userAccount": {"username": username, "password": password}}

        url = reverse("api:register")
        self.client.post(url, data, format="json")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'userAccount': {'username': ['A user with that username already exists.']}})
        self.assertEqual(Bunny.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Vegetable.objects.count(), 1)

    def test_register_single_user_duplicate_vegetable(self):
        name = "Tester"
        username = "tester"
        password = "123"
        vegetablesList = [{"vegetableType": "carrots", "value": 50}, {"vegetableType": "carrots", "value": 100}]
        data = {"name": name,
                "vegetables": vegetablesList,
                "userAccount": {"username": username, "password": password}}

        url = reverse("api:register")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_register_two_users(self):
        """tests if unable to register duplicate username"""
        name = "Tester"
        username = "tester"
        password = "123"
        vegetablesList = [{"vegetableType": "carrots", "value": 50}]
        data1 = {"name": name,
                 "vegetables": vegetablesList,
                 "userAccount": {"username": username, "password": password}}

        data2 = {"name": "tester2",
                 "vegetables": vegetablesList,
                 "userAccount": {"username": "tester2", "password": password}}

        url = reverse("api:register")
        response1 = self.client.post(url, data1, format="json")
        response2 = self.client.post(url, data2, format="json")

        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response2.status_code, status.HTTP_201_CREATED)

        self.assertEqual(response1.data, data1)
        self.assertEqual(response2.data, data2)

        self.assertEqual(Bunny.objects.count(), 2)
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(Vegetable.objects.count(), 2)

    def test_register_single_user_no_passwd(self):
        name = "Tester"
        username = "tester"
        password = ""
        vegetablesList = [{"vegetableType": "carrots", "value": 50}]
        data = {"name": name,
                "vegetables": vegetablesList,
                "userAccount": {"username": username, "password": password}}

        url = reverse("api:register")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'userAccount': {'password': ['This field may not be blank.']}})
        self.assertEqual(Bunny.objects.count(), 0)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(Vegetable.objects.count(), 0)

    def test_delete_account(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")

        url = reverse("api:delete")

        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Bunny.objects.count(), 0)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(Vegetable.objects.count(), 0)

    def test_delete_account_no_login(self):
        registerDummyBunny()

        url = reverse("api:delete")

        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Bunny.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Vegetable.objects.count(), 1)


class SessionTests(APITestCase):
    client = APIClient()

    def test_login_normal(self):
        data = {"username": "test", "password": "123"}
        registerDummyBunny()

        url = reverse("api:login")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data, {'Success': 'User logged in'})
        self.assertIn('_auth_user_id', self.client.session)

    def test_login_wrong_username(self):
        data = {"username": "test_wrong", "password": "123"}
        registerDummyBunny()

        url = reverse("api:login")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'Error': 'Wrong credentials'})

    def test_login_wrong_pass(self):
        data = {"username": "test", "password": "wrong"}
        registerDummyBunny()

        url = reverse("api:login")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'Error': 'Wrong credentials'})

    def test_login_wrong_creds(self):
        data = {"username": "test_wrong", "password": "wrong"}
        registerDummyBunny()

        url = reverse("api:login")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'Error': 'Wrong credentials'})

    def test_logout_logged_in(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")

        url = reverse("api:logout")
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'Success': 'User logged out'})

    def test_logout_not_logged_in(self):
        registerDummyBunny()

        url = reverse("api:logout")
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'detail': 'Authentication credentials were not provided.'})


class BunnyModelTests(APITestCase):
    def test_has_vegetable(self):
        bunny = registerDummyBunny()
        result = bunny.hasVegetable("carrots")

        self.assertTrue(result)

    def test_has_vegetable_no_such_veg(self):
        bunny = registerDummyBunny()
        result = bunny.hasVegetable("carrots_wrong")

        self.assertFalse(result)

    def test_get_vegetable(self):
        bunny = registerDummyBunny()
        result = bunny.getVegetable("carrots")

        self.assertEqual(result.value, 50)
        self.assertEqual(result.vegetableType, "carrots")

    def test_get_vegetable_no_such_veg(self):
        bunny = registerDummyBunny()
        result = bunny.getVegetable("carrots_wrong")

        self.assertFalse(result)


class UpdateVegetablesTests(APITestCase):
    client = APIClient()

    def test_add_vegetable(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots", "changeValue": 10}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"vegetableType": "carrots", "value": 60})

    def test_add_vegetable_no_login(self):
        registerDummyBunny()
        data = {"vegetableType": "carrots", "changeValue": 10}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {'detail': 'Authentication credentials were not provided.'})

    def test_sub_vegetable(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots", "changeValue": -10}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"vegetableType": "carrots", "value": 40})

    def test_sub_vegetable_to_zero(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots", "changeValue": -50}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"vegetableType": "carrots", "value": 0})
        self.assertEqual(Vegetable.objects.count(), 0)

    def test_add_vegetable_no_such_vegetable(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots_wrong", "changeValue": 10}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'vegetableType': ['"carrots_wrong" is not a valid choice.']})

    def test_sub_vegetable_to_zero_then_fifty(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots", "changeValue": -50}

        url = reverse("api:vegetables_update")

        response1 = self.client.post(url, data=data)
        data["changeValue"] = 5
        response2 = self.client.post(url, data)

        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data, {"vegetableType": "carrots", "value": 0})
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data, {"vegetableType": "carrots", "value": 5})
        self.assertEqual(Vegetable.objects.count(), 1)

    def test_add_vegetable_wrong_value(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableType": "carrots", "changeValue": 10000}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'changeValue': ['"10000" is not a valid choice.']})

    def test_add_vegetable_with_typo(self):
        registerDummyBunny()
        self.client.login(username="test", password="123")
        data = {"vegetableTypa": "carrots", "changeValue": 10}

        url = reverse("api:vegetables_update")

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'vegetableType': ['This field is required.']})
