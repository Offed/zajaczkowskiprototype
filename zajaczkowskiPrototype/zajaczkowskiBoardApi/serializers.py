from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Bunny, Vegetable

vegetableChoices = settings.VEGETABLES_CHOICES


class UserSerializer(serializers.ModelSerializer):
    """Used in registration process for validation"""

    class Meta:
        model = User
        fields = ("username", "password")


class VegetableUpdateSerializer(serializers.Serializer):
    """Used for updating vegetable count"""
    vegetableType = serializers.ChoiceField(choices=vegetableChoices)
    changeValue = serializers.ChoiceField(choices=[1, -1, 2, -2, 5, -5, 10, -10, 20, -20, 50, -50])


class VegetableSerializer(serializers.ModelSerializer):
    """Used for displaying vegetables, for example in list view"""

    class Meta:
        model = Vegetable
        fields = ("vegetableType", "value")


class VegetableBunnyCorrelatedSerializer(serializers.Serializer):
    """Used for displaying vegetables, for example in list view.
       Serializes by hand to avoid N+1 queries with nested serializers"""

    def to_representation(self, instance):
        data = super(VegetableBunnyCorrelatedSerializer, self).to_representation(instance)
        data["name"] = instance.bunny.name
        data["vegetables"] = {"vegetableType": instance.vegetableType, "value": instance.value}
        return data


class BunnySerializerFull(serializers.ModelSerializer):
    """Used in registration process"""
    userAccount = UserSerializer(many=False, read_only=False)
    vegetables = VegetableSerializer(many=True, required=False)

    class Meta:
        model = Bunny
        fields = ("name", "vegetables", "userAccount")
