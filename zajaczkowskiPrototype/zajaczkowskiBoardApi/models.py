from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.conf import settings

vegetableChoices = settings.VEGETABLES_CHOICES_TUPLE


# Create your models here.

class Bunny(models.Model):
    """Bunny model for bunny usage"""

    def __str__(self):
        return self.name + " " + str(list(self.vegetables.all()))

    name = models.CharField("Name", max_length=50)
    userAccount = models.ForeignKey(User, on_delete=models.CASCADE)

    def hasVegetable(self, vegetableType):  # method to check if bunny has vegetable, currently unused
        for vegetable in self.vegetables.all():
            if vegetable.vegetableType == vegetableType:
                return True
        return False

    def getVegetable(self, vegetableType):  # method to fetch bunny's vegetable by name
        for vegetable in self.vegetables.all():
            if vegetable.vegetableType == vegetableType:
                return vegetable
        return False


class Vegetable(models.Model):
    """Vegetable model for storing vegetable counts"""

    def __str__(self):
        return self.vegetableType + ":" + str(self.value)

    class Meta:
        unique_together = [  # allow only one vegetable object of a type per bunny
            ('vegetableType', 'bunny'),
        ]

    vegetableType = models.CharField(max_length=30, choices=vegetableChoices)
    value = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
    bunny = models.ForeignKey(Bunny, related_name="vegetables", on_delete=models.CASCADE)
