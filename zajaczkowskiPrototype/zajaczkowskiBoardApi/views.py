from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db import IntegrityError
from rest_framework import status, serializers
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Bunny, Vegetable
from .serializers import BunnySerializerFull, UserSerializer, VegetableUpdateSerializer, \
    VegetableBunnyCorrelatedSerializer
from .sorting import heapsort

vegetablesChoices = settings.VEGETABLES_CHOICES


# Create your views here.

@api_view(["POST"])
def registerBunny(request):
    """Register bunny. Takes name, array of vegetables, and account details"""
    serialized = BunnySerializerFull(data=request.data)
    if serialized.is_valid():
        accountDetails = serialized.validated_data['userAccount']  # pull username and password
        try:
            vegetables = serialized.validated_data["vegetables"]  # if there are initial vegetables, get them
        except KeyError:
            vegetables = []

        userToAdd = User(username=accountDetails["username"])
        userToAdd.set_password(accountDetails['password'])
        userToAdd.save()  # add user

        bunny = Bunny(name=serialized.data['name'], userAccount=userToAdd)
        bunny.save()  # create bunny

        for vegetable in vegetables:  # create vegetables, don't create if initial value is 0 to save space
            if vegetable["value"] != 0:
                try:
                    bunnyVegetable = Vegetable(vegetableType=vegetable["vegetableType"], value=vegetable["value"])
                    bunnyVegetable.bunny = bunny
                    bunnyVegetable.save()
                except IntegrityError:
                    raise serializers.ValidationError("Duplicated vegetables"
                                                      " declarations are not allowed. Error %s" % vegetable)

        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def deleteBunny(request):
    """Delete bunny account, along with his vegetables and user model"""
    user = request.user
    bunny = user.bunny_set.all()[0]
    bunny.delete()
    user.delete()

    return Response({"Success": "User deleted"}, status=status.HTTP_200_OK)


@api_view(["POST"])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def logoutBunny(request):
    """Logout function for bunny"""
    logout(request)

    return Response({"Success": "User logged out"}, status=status.HTTP_200_OK)


@api_view(["POST"])
def loginBunny(request):
    """Login view for session based auth. Requires username and password"""
    serialized = UserSerializer(data=request.data)
    try:
        nick = serialized.initial_data["username"]
        passw = serialized.initial_data["password"]
        user = authenticate(username=nick, password=passw)  # try to auth user

        if user is not None:
            login(request, user)
            return Response({"Success": "User logged in"}, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({"Error": "Wrong credentials"}, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        raise serializers.ValidationError("Username and password should be set")


@api_view(["POST"])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def updateVegetables(request):
    """View used for updating vegetables counts, one vegetable update per request.
        Uses only values +- 1,2,5,10,20,50. Negative value means decrease by value.
        Requires vegetableType and changeValue"""
    serialized = VegetableUpdateSerializer(data=request.data)
    if serialized.is_valid():
        user = request.user
        bunny = user.bunny_set.all()[0]  # get bunny object by referencing session's user

        vegetable = bunny.getVegetable(
            serialized.validated_data["vegetableType"])  # get bunny's vegetable by vegetable type

        if vegetable:  # if bunny already has vegetable object of this type
            vegetableCount = vegetable.value
            vegetableCount += serialized.validated_data["changeValue"]
        else:  # if bunny has to get new vegetable object
            vegetable = Vegetable(vegetableType=serialized.validated_data["vegetableType"], value=0, bunny=bunny)
            vegetableCount = serialized.validated_data["changeValue"]

        if vegetableCount < 0:  # sanity check
            raise serializers.ValidationError("Cannot set vegetable count negative")
        elif vegetableCount == 0:  # to save space, delete vegetables with 0 value
            vegetable.delete()
        else:
            vegetable.value = vegetableCount  # save vegetable
            vegetable.save()

        return Response({"vegetableType": serialized.validated_data["vegetableType"],
                         "value": vegetableCount}, status=status.HTTP_200_OK)
    else:
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def bunnyList(request, vegetableType):
    """ Displays heap-sorted list of bunnies, in decreasing order.
        Takes word after list ("/list/xxx") as argument to determine
        which vegetable list to display"""
    if vegetableType in vegetablesChoices:  # vegetable choices defined in settings
        vegetables = Vegetable.objects.filter(vegetableType=vegetableType).select_related('bunny')
        vegetables = list(vegetables)  # force into list that can be sorted

        if len(vegetables) == 0:
            return Response({"No bunnies": "there is 0 bunnies with this vegetable"},
                            status=status.HTTP_204_NO_CONTENT)

        heapsort(vegetables)  # heapsort call, works in place

        serialized = VegetableBunnyCorrelatedSerializer(vegetables, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)
    else:
        raise serializers.ValidationError("No such vegetable. Available are: " + ", ".join(vegetablesChoices))
