from django.conf.urls import url
from . import views

app_name = "api"
urlpatterns = [
    url(r"^bunny/register/$", views.registerBunny, name="register"),
    url(r"^bunny/delete/$", views.deleteBunny, name="delete"),
    url(r"^bunny/login/$", views.loginBunny, name="login"),
    url(r"^bunny/logout/$", views.logoutBunny, name="logout"),
    url(r"^bunny/vegetablesUpdate/$", views.updateVegetables, name="vegetables_update"),
    url(r"^list/(?P<vegetableType>[a-z]+)/$", views.bunnyList, name="list")
]
