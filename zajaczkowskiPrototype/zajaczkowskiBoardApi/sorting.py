def heapsort(vegetables):
    """Heapsort function for vegetables, works in place, descending"""

    for start in range((len(vegetables) - 2) // 2, -1, -1):
        siftdown(vegetables, start, len(vegetables) - 1)

    for end in range(len(vegetables) - 1, 0, -1):
        vegetables[end], vegetables[0] = vegetables[0], vegetables[end]
        siftdown(vegetables, 0, end - 1)
    return vegetables


def siftdown(vegetables, start, end):
    """helper function for heapsort"""
    root = start
    while True:
        child = root * 2 + 1
        if child > end:
            break
        if child + 1 <= end and vegetables[child].value > vegetables[child+1].value:
            child += 1
        if vegetables[root].value > vegetables[child].value:
            vegetables[root], vegetables[child] = vegetables[child], vegetables[root]
            root = child
        else:
            break
