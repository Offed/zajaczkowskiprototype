## **ZajaczkowskiBoardApi** ##


----------
The api allows rabbits for easy management of their vegetables, carrots at the moment, and allow to easily extend to other vegetables. The rabbits can make and account, login, update their vegetable counts and list all other rabbits. Authentication can be session-based or HTTP-basic.

*Bunny management*
------------------

**Register view**

**bunny/register/*

Method: POST

Allows for bunny registration. Requires bunny name, accountDetails (username and password) and an array of starting vegetebles, though this is not required. On success responds with mirrored request data and status code 201, created.
Example request data:

    {
	    "name" : "Bunneh",
	    "vegetables": [
	        {
	            "vegetableType": "carrots",
	            "value": 50
		    }],
	    "userAccount" : {
	        "username" : "bunneh",
	        "password" : "123"
	    }
    }

**Login view**

**bunny/login/*

Method: POST

Allows for bunny login to access update and delete functionality.  Requires username and password. Responds with 202 Accepted on success and 403 Forbidden on failure.
Example login request data:

    {
	        "username" : "bunneh",
	        "password" : "123"
    }
  
**Logout view**

**bunny/logout/*

Method: POST

Allows for bunny logout of session. Takes no data. Responds with 200 OK on success and 403 forbidden if not logged in.

**Delete account view**

**bunny/delete/*

Method: POST

Allows for bunny deletion of his own account. Deletes his vegetables too. Requires authentication. Responds with 200 OK on success and 403 forbidden if not logged in.


# *Vegetables update*#


**Subtract or add vegetables**

**bunny/vegetablesUpdate/*

Method: POST

Allows for bunny management of his own vegetables. Requires vegetableType and changeValue. Negative changeValue subtracts from vegetable count. To ensure no cheating bunnies are able to subtract/add only in portions of 1,2,5,10,20 and 50. Only a single change per request is allowed (no arrays). Requires authentication. Responds with 200 OK on success and 403 forbidden if not logged in.
Example request data (add 50 to carrots):

    {
    "vegetableType" : "carrots",
    "changeValue" : 50
    }

# *Listing bunnies*#

**Listing bunnies, sorted**

**list/vegetableType*

Method: GET

Allows bunnies to see their and other rabbits vegetable counts, sorted in descending order. No data or authentication required.

## How to run? ##
Dependencies:

    django
    djangorestframework
   
To run just clone the repo, run migrations, and run ./manage runserver or use other ways of deployment (gunicorn, apache2 modWsgi)

To use the zajaczkowskiBoardApi app in other project add "zajaczkowskiBoardApi" and "rest_framework" to INSTALLED_APPS array in settings. You should also set rest framworks auth classes. The last step should be to set vegetables that can be used in the app. The tuple variable is used in models, the array one is used in serializers.

    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        )
    }

    VEGETABLES_CHOICES_TUPLE = [("carrots", "Carrots")]
    VEGETABLES_CHOICES = ["carrots"]

------
Project includes a json fixture to populate the database. After performing initial migrations use manage.py loaddata to import it. The same fixture is used for performance testing.


To exclude slow performance tests use 'manage.py test --exclude tag "slow"'